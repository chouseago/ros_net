
(cl:in-package :asdf)

(defsystem "detection_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :sensor_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "Detection" :depends-on ("_package_Detection"))
    (:file "_package_Detection" :depends-on ("_package"))
    (:file "MaskedFrame" :depends-on ("_package_MaskedFrame"))
    (:file "_package_MaskedFrame" :depends-on ("_package"))
  ))