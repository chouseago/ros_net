
(cl:in-package :asdf)

(defsystem "masker-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :detection_msgs-msg
               :sensor_msgs-msg
)
  :components ((:file "_package")
    (:file "AddTwoInts" :depends-on ("_package_AddTwoInts"))
    (:file "_package_AddTwoInts" :depends-on ("_package"))
    (:file "MaskImage" :depends-on ("_package_MaskImage"))
    (:file "_package_MaskImage" :depends-on ("_package"))
  ))