; Auto-generated. Do not edit!


(cl:in-package masker-srv)


;//! \htmlinclude MaskImage-request.msg.html

(cl:defclass <MaskImage-request> (roslisp-msg-protocol:ros-message)
  ((frame
    :reader frame
    :initarg :frame
    :type sensor_msgs-msg:Image
    :initform (cl:make-instance 'sensor_msgs-msg:Image)))
)

(cl:defclass MaskImage-request (<MaskImage-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MaskImage-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MaskImage-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name masker-srv:<MaskImage-request> is deprecated: use masker-srv:MaskImage-request instead.")))

(cl:ensure-generic-function 'frame-val :lambda-list '(m))
(cl:defmethod frame-val ((m <MaskImage-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader masker-srv:frame-val is deprecated.  Use masker-srv:frame instead.")
  (frame m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MaskImage-request>) ostream)
  "Serializes a message object of type '<MaskImage-request>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'frame) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MaskImage-request>) istream)
  "Deserializes a message object of type '<MaskImage-request>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'frame) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MaskImage-request>)))
  "Returns string type for a service object of type '<MaskImage-request>"
  "masker/MaskImageRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MaskImage-request)))
  "Returns string type for a service object of type 'MaskImage-request"
  "masker/MaskImageRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MaskImage-request>)))
  "Returns md5sum for a message object of type '<MaskImage-request>"
  "cc786a9377592850207941244450d0ba")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MaskImage-request)))
  "Returns md5sum for a message object of type 'MaskImage-request"
  "cc786a9377592850207941244450d0ba")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MaskImage-request>)))
  "Returns full string definition for message of type '<MaskImage-request>"
  (cl:format cl:nil "sensor_msgs/Image frame~%~%================================================================================~%MSG: sensor_msgs/Image~%# This message contains an uncompressed image~%# (0, 0) is at top-left corner of image~%#~%~%Header header        # Header timestamp should be acquisition time of image~%                     # Header frame_id should be optical frame of camera~%                     # origin of frame should be optical center of camera~%                     # +x should point to the right in the image~%                     # +y should point down in the image~%                     # +z should point into to plane of the image~%                     # If the frame_id here and the frame_id of the CameraInfo~%                     # message associated with the image conflict~%                     # the behavior is undefined~%~%uint32 height         # image height, that is, number of rows~%uint32 width          # image width, that is, number of columns~%~%# The legal values for encoding are in file src/image_encodings.cpp~%# If you want to standardize a new string format, join~%# ros-users@lists.sourceforge.net and send an email proposing a new encoding.~%~%string encoding       # Encoding of pixels -- channel meaning, ordering, size~%                      # taken from the list of strings in include/sensor_msgs/image_encodings.h~%~%uint8 is_bigendian    # is this data bigendian?~%uint32 step           # Full row length in bytes~%uint8[] data          # actual matrix data, size is (step * rows)~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MaskImage-request)))
  "Returns full string definition for message of type 'MaskImage-request"
  (cl:format cl:nil "sensor_msgs/Image frame~%~%================================================================================~%MSG: sensor_msgs/Image~%# This message contains an uncompressed image~%# (0, 0) is at top-left corner of image~%#~%~%Header header        # Header timestamp should be acquisition time of image~%                     # Header frame_id should be optical frame of camera~%                     # origin of frame should be optical center of camera~%                     # +x should point to the right in the image~%                     # +y should point down in the image~%                     # +z should point into to plane of the image~%                     # If the frame_id here and the frame_id of the CameraInfo~%                     # message associated with the image conflict~%                     # the behavior is undefined~%~%uint32 height         # image height, that is, number of rows~%uint32 width          # image width, that is, number of columns~%~%# The legal values for encoding are in file src/image_encodings.cpp~%# If you want to standardize a new string format, join~%# ros-users@lists.sourceforge.net and send an email proposing a new encoding.~%~%string encoding       # Encoding of pixels -- channel meaning, ordering, size~%                      # taken from the list of strings in include/sensor_msgs/image_encodings.h~%~%uint8 is_bigendian    # is this data bigendian?~%uint32 step           # Full row length in bytes~%uint8[] data          # actual matrix data, size is (step * rows)~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MaskImage-request>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'frame))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MaskImage-request>))
  "Converts a ROS message object to a list"
  (cl:list 'MaskImage-request
    (cl:cons ':frame (frame msg))
))
;//! \htmlinclude MaskImage-response.msg.html

(cl:defclass <MaskImage-response> (roslisp-msg-protocol:ros-message)
  ((segmentation
    :reader segmentation
    :initarg :segmentation
    :type detection_msgs-msg:MaskedFrame
    :initform (cl:make-instance 'detection_msgs-msg:MaskedFrame)))
)

(cl:defclass MaskImage-response (<MaskImage-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MaskImage-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MaskImage-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name masker-srv:<MaskImage-response> is deprecated: use masker-srv:MaskImage-response instead.")))

(cl:ensure-generic-function 'segmentation-val :lambda-list '(m))
(cl:defmethod segmentation-val ((m <MaskImage-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader masker-srv:segmentation-val is deprecated.  Use masker-srv:segmentation instead.")
  (segmentation m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MaskImage-response>) ostream)
  "Serializes a message object of type '<MaskImage-response>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'segmentation) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MaskImage-response>) istream)
  "Deserializes a message object of type '<MaskImage-response>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'segmentation) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MaskImage-response>)))
  "Returns string type for a service object of type '<MaskImage-response>"
  "masker/MaskImageResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MaskImage-response)))
  "Returns string type for a service object of type 'MaskImage-response"
  "masker/MaskImageResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MaskImage-response>)))
  "Returns md5sum for a message object of type '<MaskImage-response>"
  "cc786a9377592850207941244450d0ba")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MaskImage-response)))
  "Returns md5sum for a message object of type 'MaskImage-response"
  "cc786a9377592850207941244450d0ba")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MaskImage-response>)))
  "Returns full string definition for message of type '<MaskImage-response>"
  (cl:format cl:nil "detection_msgs/MaskedFrame segmentation~%~%~%================================================================================~%MSG: detection_msgs/MaskedFrame~%Header header~%detection_msgs/Detection[] detections~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: detection_msgs/Detection~%Header header~%sensor_msgs/Image mask~%uint32 classID~%float64 score~%~%================================================================================~%MSG: sensor_msgs/Image~%# This message contains an uncompressed image~%# (0, 0) is at top-left corner of image~%#~%~%Header header        # Header timestamp should be acquisition time of image~%                     # Header frame_id should be optical frame of camera~%                     # origin of frame should be optical center of camera~%                     # +x should point to the right in the image~%                     # +y should point down in the image~%                     # +z should point into to plane of the image~%                     # If the frame_id here and the frame_id of the CameraInfo~%                     # message associated with the image conflict~%                     # the behavior is undefined~%~%uint32 height         # image height, that is, number of rows~%uint32 width          # image width, that is, number of columns~%~%# The legal values for encoding are in file src/image_encodings.cpp~%# If you want to standardize a new string format, join~%# ros-users@lists.sourceforge.net and send an email proposing a new encoding.~%~%string encoding       # Encoding of pixels -- channel meaning, ordering, size~%                      # taken from the list of strings in include/sensor_msgs/image_encodings.h~%~%uint8 is_bigendian    # is this data bigendian?~%uint32 step           # Full row length in bytes~%uint8[] data          # actual matrix data, size is (step * rows)~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MaskImage-response)))
  "Returns full string definition for message of type 'MaskImage-response"
  (cl:format cl:nil "detection_msgs/MaskedFrame segmentation~%~%~%================================================================================~%MSG: detection_msgs/MaskedFrame~%Header header~%detection_msgs/Detection[] detections~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: detection_msgs/Detection~%Header header~%sensor_msgs/Image mask~%uint32 classID~%float64 score~%~%================================================================================~%MSG: sensor_msgs/Image~%# This message contains an uncompressed image~%# (0, 0) is at top-left corner of image~%#~%~%Header header        # Header timestamp should be acquisition time of image~%                     # Header frame_id should be optical frame of camera~%                     # origin of frame should be optical center of camera~%                     # +x should point to the right in the image~%                     # +y should point down in the image~%                     # +z should point into to plane of the image~%                     # If the frame_id here and the frame_id of the CameraInfo~%                     # message associated with the image conflict~%                     # the behavior is undefined~%~%uint32 height         # image height, that is, number of rows~%uint32 width          # image width, that is, number of columns~%~%# The legal values for encoding are in file src/image_encodings.cpp~%# If you want to standardize a new string format, join~%# ros-users@lists.sourceforge.net and send an email proposing a new encoding.~%~%string encoding       # Encoding of pixels -- channel meaning, ordering, size~%                      # taken from the list of strings in include/sensor_msgs/image_encodings.h~%~%uint8 is_bigendian    # is this data bigendian?~%uint32 step           # Full row length in bytes~%uint8[] data          # actual matrix data, size is (step * rows)~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MaskImage-response>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'segmentation))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MaskImage-response>))
  "Converts a ROS message object to a list"
  (cl:list 'MaskImage-response
    (cl:cons ':segmentation (segmentation msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'MaskImage)))
  'MaskImage-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'MaskImage)))
  'MaskImage-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MaskImage)))
  "Returns string type for a service object of type '<MaskImage>"
  "masker/MaskImage")