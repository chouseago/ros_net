
"use strict";

let Detection = require('./Detection.js');
let MaskedFrame = require('./MaskedFrame.js');

module.exports = {
  Detection: Detection,
  MaskedFrame: MaskedFrame,
};
