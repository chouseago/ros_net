# CMake generated Testfile for 
# Source directory: /home/charlie/ros_net/catkin_ws/src
# Build directory: /home/charlie/ros_net/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("detection_msgs")
subdirs("masker")
