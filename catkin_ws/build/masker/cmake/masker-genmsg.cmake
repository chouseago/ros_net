# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "masker: 0 messages, 2 services")

set(MSG_I_FLAGS "-Idetection_msgs:/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/kinetic/share/sensor_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/kinetic/share/geometry_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(masker_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv" NAME_WE)
add_custom_target(_masker_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "masker" "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv" "detection_msgs/MaskedFrame:sensor_msgs/Image:std_msgs/Header:detection_msgs/Detection"
)

get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv" NAME_WE)
add_custom_target(_masker_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "masker" "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages

### Generating Services
_generate_srv_cpp(masker
  "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv"
  "${MSG_I_FLAGS}"
  "/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg/MaskedFrame.msg;/opt/ros/kinetic/share/sensor_msgs/cmake/../msg/Image.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg/Detection.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/masker
)
_generate_srv_cpp(masker
  "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/masker
)

### Generating Module File
_generate_module_cpp(masker
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/masker
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(masker_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(masker_generate_messages masker_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv" NAME_WE)
add_dependencies(masker_generate_messages_cpp _masker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv" NAME_WE)
add_dependencies(masker_generate_messages_cpp _masker_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(masker_gencpp)
add_dependencies(masker_gencpp masker_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS masker_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages

### Generating Services
_generate_srv_eus(masker
  "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv"
  "${MSG_I_FLAGS}"
  "/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg/MaskedFrame.msg;/opt/ros/kinetic/share/sensor_msgs/cmake/../msg/Image.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg/Detection.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/masker
)
_generate_srv_eus(masker
  "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/masker
)

### Generating Module File
_generate_module_eus(masker
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/masker
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(masker_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(masker_generate_messages masker_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv" NAME_WE)
add_dependencies(masker_generate_messages_eus _masker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv" NAME_WE)
add_dependencies(masker_generate_messages_eus _masker_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(masker_geneus)
add_dependencies(masker_geneus masker_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS masker_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages

### Generating Services
_generate_srv_lisp(masker
  "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv"
  "${MSG_I_FLAGS}"
  "/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg/MaskedFrame.msg;/opt/ros/kinetic/share/sensor_msgs/cmake/../msg/Image.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg/Detection.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/masker
)
_generate_srv_lisp(masker
  "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/masker
)

### Generating Module File
_generate_module_lisp(masker
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/masker
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(masker_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(masker_generate_messages masker_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv" NAME_WE)
add_dependencies(masker_generate_messages_lisp _masker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv" NAME_WE)
add_dependencies(masker_generate_messages_lisp _masker_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(masker_genlisp)
add_dependencies(masker_genlisp masker_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS masker_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages

### Generating Services
_generate_srv_nodejs(masker
  "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv"
  "${MSG_I_FLAGS}"
  "/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg/MaskedFrame.msg;/opt/ros/kinetic/share/sensor_msgs/cmake/../msg/Image.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg/Detection.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/masker
)
_generate_srv_nodejs(masker
  "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/masker
)

### Generating Module File
_generate_module_nodejs(masker
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/masker
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(masker_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(masker_generate_messages masker_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv" NAME_WE)
add_dependencies(masker_generate_messages_nodejs _masker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv" NAME_WE)
add_dependencies(masker_generate_messages_nodejs _masker_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(masker_gennodejs)
add_dependencies(masker_gennodejs masker_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS masker_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages

### Generating Services
_generate_srv_py(masker
  "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv"
  "${MSG_I_FLAGS}"
  "/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg/MaskedFrame.msg;/opt/ros/kinetic/share/sensor_msgs/cmake/../msg/Image.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/charlie/ros_net/catkin_ws/src/detection_msgs/msg/Detection.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/masker
)
_generate_srv_py(masker
  "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/masker
)

### Generating Module File
_generate_module_py(masker
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/masker
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(masker_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(masker_generate_messages masker_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/MaskImage.srv" NAME_WE)
add_dependencies(masker_generate_messages_py _masker_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/charlie/ros_net/catkin_ws/src/masker/srv/AddTwoInts.srv" NAME_WE)
add_dependencies(masker_generate_messages_py _masker_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(masker_genpy)
add_dependencies(masker_genpy masker_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS masker_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/masker)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/masker
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET detection_msgs_generate_messages_cpp)
  add_dependencies(masker_generate_messages_cpp detection_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(masker_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/masker)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/masker
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET detection_msgs_generate_messages_eus)
  add_dependencies(masker_generate_messages_eus detection_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(masker_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/masker)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/masker
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET detection_msgs_generate_messages_lisp)
  add_dependencies(masker_generate_messages_lisp detection_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(masker_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/masker)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/masker
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET detection_msgs_generate_messages_nodejs)
  add_dependencies(masker_generate_messages_nodejs detection_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(masker_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/masker)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/masker\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/masker
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET detection_msgs_generate_messages_py)
  add_dependencies(masker_generate_messages_py detection_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(masker_generate_messages_py std_msgs_generate_messages_py)
endif()
