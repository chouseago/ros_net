//
// Created by charliehouseago on 09/02/19
//

#ifndef ROS_CV_SEGMENTOR_H
#define ROS_CV_SEGMENTOR_H

#include <map>
#include <vector>
#include <eigen3/Eigen/Dense>
#include <utility>

// OpenCV Includes
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

// Ros includes
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <std_msgs/Bool.h>
#include <detection_msgs/Detection.h>
#include <detection_msgs/MaskedFrame.h>
#include <masker/MaskImage.h>

// The MaskR class handles all of the backend ROS Communication,
// it contains the ros nodes and functions for communicating with python
class MaskR {
public:
    MaskR();
    ~MaskR();

    bool processingLock = false;
    std::vector<std::pair<cv::Mat,double>> Process(const cv::Mat& img);

private:
    ros::NodeHandle nh_;
    ros::ServiceClient srvClient;
};

//
// The Segmentor class is a wrapper class for handling calls to a Mask-RCNN script running in python.
// Communication is handled over Ros therefore all required functionality is wrapped into this class.
// The partner code to be run in python is in scripts/segmentor.py
//
class Segmentor {
public:

    // Default Constructor
    Segmentor(int argc, char** argv);
    ~Segmentor();

    // Process CV:Mat
    std::vector<std::pair<cv::Mat,double>> ProcessFrame(const cv::Mat& input);

    MaskR* masker;
};


#endif //ROS_CV_SEGMENTOR_H
