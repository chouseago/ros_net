#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from collections import defaultdict
import argparse
import sys
import rospy
import cv2
import glob
import logging
import os
import time

from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

from caffe2.python import workspace

from detectron.core.config import assert_and_infer_cfg
from detectron.core.config import cfg
from detectron.core.config import merge_cfg_from_file
from detectron.utils.io import cache_url
from detectron.utils.logging import setup_logging
from detectron.utils.timer import Timer
import detectron.core.test_engine as infer_engine
import detectron.datasets.dummy_datasets as dummy_datasets
import detectron.utils.c2 as c2_utils
import detectron.utils.vis as vis_utils

c2_utils.import_detectron_ops()
cv2.ocl.setUseOpenCL(False)

class image_converter:

  def __init__(self):
    # Setup Ros Nodes for image passing
    # -----------------------------------------------
    self.image_pub = rospy.Publisher("/segmentor/frompython",Image, queue_size=1)

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/segmentor/topython",Image,self.callback)
    # -----------------------------------------------

  def callback(self,data):
    print('Data Received')
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError as e:
      print(e)

    (rows,cols,channels) = cv_image.shape

    # Now Process Image through Mask-RCNN
    # -----------------------------------------------
    timers = defaultdict(Timer)
    t = time.time()
    with c2_utils.NamedCudaScope(0):
        cls_boxes, cls_segms, cls_keyps = infer_engine.im_detect_all(
            model, cv_image, None, timers=timers
        )
    mask_image = vis_utils.vis_one_image_opencv(
        cv_image[:, :, ::-1],  # BGR -> RGB for visualization
        cls_boxes,
        cls_segms,
        cls_keyps,
        thresh=0.7,
        kp_thresh=2.0,
        show_box=False,
        dataset=dummy_coco_dataset,
        show_class=False
    )
    # -----------------------------------------------
    print("Image Processed")

    try:
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(mask_image, "bgr8"))
    except CvBridgeError as e:
      print(e)

    print("Published...")

def get_rpn_box_proposals(im, args):
    cfg.immutable(False)
    merge_cfg_from_file(args.rpn_cfg)
    cfg.NUM_GPUS = 1
    cfg.MODEL.RPN_ONLY = True
    cfg.TEST.RPN_PRE_NMS_TOP_N = 10000
    cfg.TEST.RPN_POST_NMS_TOP_N = 2000
    assert_and_infer_cfg(cache_urls=False)

    model = model_engine.initialize_model_from_cfg(args.rpn_pkl)
    with c2_utils.NamedCudaScope(0):
        boxes, scores = rpn_engine.im_proposals(model, im)
    return boxes, scores

def main(args):

  # Initialize Mask RCNN
  # -----------------------------------------------
  merge_cfg_from_file("../configs/12_2017_baselines/e2e_mask_rcnn_R-101-FPN_2x.yaml")
  cfg.NUM_GPUS = 1
  weights = cache_url("https://dl.fbaipublicfiles.com/detectron/35861858/12_2017_baselines/e2e_mask_rcnn_R-101-FPN_2x.yaml.02_32_51.SgT4y1cO/output/train/coco_2014_train:coco_2014_valminusminival/generalized_rcnn/model_final.pkl", cfg.DOWNLOAD_CACHE)
  assert_and_infer_cfg(cache_urls=False)

  global model
  model = infer_engine.initialize_model_from_cfg(weights)
  global dummy_coco_dataset
  dummy_coco_dataset = dummy_datasets.get_coco_dataset()
  # -----------------------------------------------

  ic = image_converter()
  rospy.init_node('image_converter', anonymous=True)
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
